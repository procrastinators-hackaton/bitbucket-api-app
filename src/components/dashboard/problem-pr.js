import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  CardHeader,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@mui/material';

export const ProblematicPR = (props) => {

  const dateDiff = (date1, date2) => {
    const diffTime = Math.abs(date2 - date1);
    return Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
  } 

  return <Card {...props}>
          <CardHeader title="Problematic Pull Requests" />
          <PerfectScrollbar>
            <Box sx={{ minWidth: 800 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>
                      Author
                    </TableCell>
                    <TableCell>
                      Title
                    </TableCell>
                    <TableCell>
                      Task count
                    </TableCell>
                    <TableCell>
                      Delay day
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.pr && props.pr.slice(0, 5).map((pullRequest, idx) => (
                    <TableRow
                      hover
                      key={idx}>
                      <TableCell>
                        {pullRequest.author.display_name}
                      </TableCell>
                      <TableCell>
                        {pullRequest.title}
                      </TableCell>
                      <TableCell>
                        {pullRequest.task_count}
                      </TableCell>
                      <TableCell>
                        {dateDiff(new Date(), new Date(pullRequest.updated_on))}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-end',
              p: 2
            }}>
          </Box>
        </Card>
};
