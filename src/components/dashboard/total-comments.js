import { Avatar, Box, Card, CardContent, Grid, Typography } from '@mui/material';
import CommentIcon from '@mui/icons-material/Comment';

export const TotalCommments = (props) => (
  <Card {...props}>
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}>
        <Grid item>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="overline">
            TOTAL COMMENTS
          </Typography>
          <Typography
            color="textPrimary"
            variant="h4">
            {props.commentsCount ? props.commentsCount : 0}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: 'success.main',
              height: 56,
              width: 56
            }}>
            <CommentIcon />
          </Avatar>
        </Grid>
      </Grid>
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          pt: 2
        }}>
      </Box>
    </CardContent>
  </Card>
);
