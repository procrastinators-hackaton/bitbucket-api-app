import { Avatar, Box, Card, CardContent, Grid, LinearProgress, Typography } from '@mui/material';
import TaskIcon from '@mui/icons-material/Task';

export const TasksProgress = (props) => (
  <Card
    sx={{ height: '100%' }}
    {...props}>
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}>
        <Grid item>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="overline">
            TOTAL TASKS
          </Typography>
          <Typography
            color="textPrimary"
            variant="h4">
            {props.taskCount ? props.taskCount : 0}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: 'warning.main',
              height: 56,
              width: 56
            }}>
            <TaskIcon />
          </Avatar>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);
