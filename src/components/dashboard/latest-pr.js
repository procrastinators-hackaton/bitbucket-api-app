import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Button,
  Card,
  CardHeader,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@mui/material';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { SeverityPill } from '../severity-pill';


export const LatestPR = (props) => {

  return <Card {...props}>
          <CardHeader title="Latest Pull Requests" />
          <PerfectScrollbar>
            <Box sx={{ minWidth: 800 }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>
                      Author
                    </TableCell>
                    <TableCell>
                      Title
                    </TableCell>
                    <TableCell>
                      Task count
                    </TableCell>
                    <TableCell>
                      Status
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.pr && props.pr.values.slice(0, 5).map((pullRequest, idx) => (
                    <TableRow
                      hover
                      key={idx}>
                      <TableCell>
                        {pullRequest.author.display_name}
                      </TableCell>
                      <TableCell>
                        {pullRequest.title}
                      </TableCell>
                      <TableCell>
                        {pullRequest.task_count}
                      </TableCell>
                      <TableCell>
                        <SeverityPill
                          color={(pullRequest.state === 'OPEN' && 'info')
                          || (pullRequest.state === 'DECLINED' && 'error')
                          || 'success'}>
                          {pullRequest.state}
                        </SeverityPill>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-end',
              p: 2
            }}>
            <Button
              color="primary"
              onClick={() => window.open("https://bitbucket.org/procrastinators-hackaton/test-app/pull-requests/", "_blank")}
              endIcon={<ArrowRightIcon fontSize="small" />}
              size="small"
              variant="text">
              View all
            </Button>
          </Box>
        </Card>
};
