import { Doughnut } from 'react-chartjs-2';
import { Box, Card, CardContent, CardHeader, Divider, Typography, useTheme } from '@mui/material';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import PhoneIcon from '@mui/icons-material/Phone';
import TabletIcon from '@mui/icons-material/Tablet';


function filterByUsers(users) {
  if (!users) {
    return [];
  }

  return users.map((user) => user.user.display_name)
}

function filterPRByUser(pr, users) {
  if (!users || !pr) {
    return [];
  }

  let totalPr = pr.length;
  let result = [];
  for (let user of users) {
    result.push(pr.filter(pr => pr.author.display_name === user.user.display_name).length / totalPr * 100);
  }

  return result;
}

export const PRByPerson = (props) => {
  const theme = useTheme();

  const data = {
    datasets: [
      {
        data: filterPRByUser(props.pr, props.users),
        backgroundColor: ['#3F51B5', '#e53935', '#FB8C00'],
        borderWidth: 8,
        borderColor: '#FFFFFF',
        hoverBorderColor: '#FFFFFF'
      }
    ],
    labels: filterByUsers(props.users)
  };

  const options = {
    animation: false,
    cutoutPercentage: 80,
    layout: { padding: 0 },
    legend: {
      display: false
    },
    maintainAspectRatio: false,
    responsive: true,
    tooltips: {
      backgroundColor: theme.palette.background.paper,
      bodyFontColor: theme.palette.text.secondary,
      borderColor: theme.palette.divider,
      borderWidth: 1,
      enabled: true,
      footerFontColor: theme.palette.text.secondary,
      intersect: false,
      mode: 'index',
      titleFontColor: theme.palette.text.primary
    }
  };

  return (
    <Card {...props}>
      <CardHeader title="Opened by person" />
      <Divider />
      <CardContent>
        <Box
          sx={{
            height: 300,
            position: 'relative'
          }}
        >
          <Doughnut
            data={data}
            options={options}
          />
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            pt: 2
          }}>

        </Box>
      </CardContent>
    </Card>
  );
};
