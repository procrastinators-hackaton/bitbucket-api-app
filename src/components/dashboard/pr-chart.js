import { Bar } from 'react-chartjs-2';
import { Box, Card, CardContent, CardHeader, Divider, useTheme } from '@mui/material';

function getDateRange(date) {
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
  if (!date) {
    return ;
  }

  let result = [];
  for (var d = new Date(date[0]); d < date[1]; d.setDate(d.getDate() + 1)) {
    result.push(d.getDate() + ' ' + monthNames[d.getMonth()]);
  }
  return result;
}

function getChartDate(state, data, dates) {
  if (!data || !dates) {
    return ;
  }

  let dateArr = [];
  for (var d = new Date(dates[0]); d < dates[1]; d.setDate(d.getDate() + 1)) {
    dateArr.push(new Date(d));
  }

  const tempPR = data.filter(pr => pr.state === state);
  let result = [];
  for (let date of dateArr) {
    let count = 0;
    for (let pr of tempPR) {
      if (compareDateWithoutTime(new Date(pr.created_on), date)) {
        ++count;
      } 
      if (pr.state === "MERGED" && compareDateWithoutTime(new Date(pr.updated_on), date)) {
        ++count;
      }
      if (pr.state === "DECLINED" && compareDateWithoutTime(new Date(pr.updated_on), date)) {
        ++count;
      }
    }
    result.push(count);
  }

  return result;
}

function compareDateWithoutTime(date1, date2) {
  if (date1.getFullYear() == date2.getFullYear() && date1.getMonth() == date2.getMonth() && date1.getDate() == date2.getDate()) {
    return true;
  } else {
    return false;
  }
}

export const PRChart = (props) => {
  const theme = useTheme();

  const data = {
    datasets: [
      {
        backgroundColor: '#FF0000',
        barPercentage: 0.5,
        barThickness: 12,
        borderRadius: 4,
        categoryPercentage: 0.5,
        data: getChartDate('DECLINED', props.chartData, props.labels),
        label: 'Declined',
        maxBarThickness: 10
      },
      {
        backgroundColor: '#00ff00',
        barPercentage: 0.5,
        barThickness: 12,
        borderRadius: 4,
        categoryPercentage: 0.5,
        data: getChartDate('MERGED', props.chartData, props.labels),
        label: 'Merged',
        maxBarThickness: 10
      },
      {
        backgroundColor: '#0000FF',
        barPercentage: 0.5,
        barThickness: 12,
        borderRadius: 4,
        categoryPercentage: 0.5,
        data: getChartDate('OPEN', props.chartData, props.labels),
        label: 'Opened',
        maxBarThickness: 10
      }
    ],
    labels: getDateRange(props.labels)
  };

  const options = {
    animation: false,
    cornerRadius: 20,
    layout: { padding: 0 },
    legend: { display: false },
    maintainAspectRatio: false,
    responsive: true,
    xAxes: [
      {
        ticks: {
          fontColor: theme.palette.text.secondary
        },
        gridLines: {
          display: false,
          drawBorder: false
        }
      }
    ],
    yAxes: [
      {
        ticks: {
          fontColor: theme.palette.text.secondary,
          beginAtZero: true,
          min: 0
        },
        gridLines: {
          borderDash: [2],
          borderDashOffset: [2],
          color: theme.palette.divider,
          drawBorder: false,
          zeroLineBorderDash: [2],
          zeroLineBorderDashOffset: [2],
          zeroLineColor: theme.palette.divider
        }
      }
    ],
    tooltips: {
      backgroundColor: theme.palette.background.paper,
      bodyFontColor: theme.palette.text.secondary,
      borderColor: theme.palette.divider,
      borderWidth: 1,
      enabled: true,
      footerFontColor: theme.palette.text.secondary,
      intersect: false,
      mode: 'index',
      titleFontColor: theme.palette.text.primary
    }
  };

  return (
    <Card {...props}>
      <CardHeader
        title="Total pull requests"
      />
      <Divider />
      <CardContent>
        <Box
          sx={{
            height: 400,
            position: 'relative'
          }}
        >
          <Bar
            data={data}
            options={options}
          />
        </Box>
      </CardContent>
      <Divider />
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end',
          p: 2
        }}
      >
      </Box>
    </Card>
  );
};
