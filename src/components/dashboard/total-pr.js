import { Avatar, Card, CardContent, Grid, Typography } from '@mui/material';
import MoneyIcon from '@mui/icons-material/Money';

export const TotalPR = (props) => (
  <Card
    sx={{ height: '100%' }}
    {...props}>
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}>
        <Grid item>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="overline">
            TOTAL PULL REQUESTS
          </Typography>
          <Typography
            color="textPrimary"
            variant="h4">
            {props.prCount ? props.prCount : 0}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: 'error.main',
              height: 56,
              width: 56
            }}>
            <MoneyIcon />
          </Avatar>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);
