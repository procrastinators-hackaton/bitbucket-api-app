export const retrieveTokenFromURL = (url) => {
    return url.substring(url.indexOf("=") + 1, url.indexOf("&"));
}

export const saveToken = (accessToken) => {
    localStorage.setItem('access_token', accessToken);
}