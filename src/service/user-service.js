import { REACT_APP_API_BITBUCKET_WORKSPACE } from '../utils/config';
import fetchRequest from '../utils/fetch-request';

export const getUserList = async () => {
    return await fetchRequest
        .get(REACT_APP_API_BITBUCKET_WORKSPACE + "/members?pagelen=50")
        .then((response) => {
            return response.data;
        });
};

