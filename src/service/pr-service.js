import { REACT_APP_API_BITBUCKET_REPO } from '../utils/config';
import fetchRequest from '../utils/fetch-request';
import { formatDate } from 'src/utils/date-formatter';

export const getPRSortedDescending = async () => {
    return await fetchRequest
        .get(REACT_APP_API_BITBUCKET_REPO + '/pullrequests?fields=-values.links&q=state="OPEN"&sort=-updated_on')
        .then((response) => {
            return response.data;
        });
};

export const getProblemPR = async () => {
    return await fetchRequest
        .get(REACT_APP_API_BITBUCKET_REPO + '/pullrequests?q=state="OPEN"&sort=updated_on&pagelen=50')
        .then((response) => {
            return response.data;
        });
};

export const getPRBetweenDateRange = async (start, end) => {
    return await fetchRequest
        .get(REACT_APP_API_BITBUCKET_REPO + `/pullrequests?q=created_on>=${formatDate(start)}+AND+created_on<=${formatDate(end)}&pagelen=50`)
        .then((response) => {
            return response.data;
        });
};

