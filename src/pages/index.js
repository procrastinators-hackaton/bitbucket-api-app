import Head from 'next/head';
import { Box, Container, Grid } from '@mui/material';
import { TotalPR } from '../components/dashboard/total-pr';
import { LatestPR } from '../components/dashboard/latest-pr';
//import { TopCommenters } from '../components/dashboard/top-commenters';
import { PRChart } from '../components/dashboard/pr-chart';
import { TasksProgress } from '../components/dashboard/tasks-progress';
import { TotalCommments } from '../components/dashboard/total-comments';
import { PRByPerson } from '../components/dashboard/pr-by-people';
import { DashboardLayout } from '../components/dashboard-layout';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { retrieveTokenFromURL, saveToken } from '../utils/token-utils';
import { getPRSortedDescending, getPRBetweenDateRange, getProblemPR } from '../service/pr-service';
import { getUserList } from 'src/service/user-service';
import * as React from 'react';
import DateRangePicker from '@mui/lab/DateRangePicker';
import DateFnsAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import TextField from '@mui/material/TextField';
import { REACT_APP_API_KEY } from 'src/utils/config';
import { ProblematicPR } from 'src/components/dashboard/problem-pr';


const Dashboard = () => {

  const today = new Date();
  const router = useRouter();
  const reducer = (accumulator, curr) => accumulator + curr;

  const [pr, setPR] = useState(null);
  const [formatPr, setFormatPR] = useState(null);
  const [problemPR, setProblemPR] = useState(null);
  const [date, setDate] = useState([new Date().setDate(today.getDate() - 3), today]);
  const [users, setUsers] = useState(null);

  useEffect(() => {
    const url = window.location.hash;
    if (url.includes("access_token")) {
      saveToken(retrieveTokenFromURL(url));
      router.replace("/");
    } else if (localStorage.getItem("access_token") === null) {
      window.location.href = `https://bitbucket.org/site/oauth2/authorize?client_id=${REACT_APP_API_KEY}&response_type=token`;
    } else {
      getPRSortedDescending()
        .then(response => setPR(response));
      getPRBetweenDateRange(date[0], date[1])
        .then(response => setFormatPR(response));
      getProblemPR()
        .then(response => setProblemPR(response));
      getUserList()
        .then(response => setUsers(response));
    }
  }, []);


  return <>
    <Head>
      <title>
        Procrastinators
      </title>
    </Head>
    <Box
      component="main"
      sx={{
        flexGrow: 10,
        py: 8
      }}>
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}>
          <Grid
            item
            lg={3}
            sm={6}
            xl={3}
            xs={12}>
              <LocalizationProvider dateAdapter={DateFnsAdapter}>
              <DateRangePicker
                startText="Check-in"
                endText="Check-out"
                value={date}
                onChange={(newValue) => {
                  setDate(newValue);
                  getPRBetweenDateRange(newValue)
                    .then(response => setFormatPR(response));
                }}
                renderInput={(startProps, endProps) => (
                  <React.Fragment>
                    <TextField {...startProps} />
                    <Box sx={{ mx: 2 }}> to </Box>
                    <TextField {...endProps} />
                  </React.Fragment>
                )}
              />
              </LocalizationProvider> 
          </Grid>
          <Grid
            item
            xl={3}
            lg={3}
            sm={6}
            xs={12}>
            <TotalCommments commentsCount={formatPr && formatPr.values.map(pr => pr.comment_count).reduce(reducer)}/>
          </Grid>
          <Grid
            item
            xl={3}
            lg={3}
            sm={6}
            xs={12}>
            <TasksProgress taskCount={formatPr && formatPr.values.map(pr => pr.task_count).reduce(reducer)}/>
          </Grid>
          <Grid
            item
            xl={3}
            lg={3}
            sm={6}
            xs={12}>
            <TotalPR sx={{ height: '100%' }} prCount={formatPr && formatPr.size} />
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}>
            <PRChart labels={date} chartData={formatPr && formatPr.values}/>
          </Grid>
          <Grid
            item
            lg={4}
            md={6}
            xl={3}
            xs={12}>
            <PRByPerson sx={{ height: '100%' }} users={users && users.values} pr={pr && pr.values}/>
          </Grid>
          <Grid
            item
            lg={4}
            md={6}
            xl={3}
            xs={12}>
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}>
            <LatestPR pr={pr} />
          </Grid>
          <Grid
            item
            lg={8}
            md={12}
            xl={9}
            xs={12}>
            <ProblematicPR pr={problemPR && problemPR.values} />
          </Grid>
        </Grid>
      </Container>
    </Box>
  </>
};

Dashboard.getLayout = (page) => (
  <DashboardLayout>
    {page}
  </DashboardLayout>
);

export default Dashboard;
